rootProject.name = "alq-gateway"

pluginManagement {
    repositories {
        mavenLocal()
        maven(url = "https://nexus.jeikobu.net/repository/maven-releases/")
        gradlePluginPortal()
    }
}
