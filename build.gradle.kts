import org.springframework.boot.gradle.tasks.bundling.BootBuildImage
import org.aliquam.AlqUtils

plugins {
	id("org.springframework.boot") version "2.5.5"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	id("java")
	id("org.sonarqube") version "3.3"
	jacoco
	id("io.freefair.lombok") version "6.2.0"
	id("org.aliquam.alq-gradle-parent") version "0.4.14"
}
val alq = AlqUtils(project).withStandardProjectSetup()

group = "org.aliquam"
val artifactId = "alq-gateway"
val baseVersion = "0.0.1"
version = alq.getSemVersion(baseVersion)

val branchName: String? = System.getenv("BRANCH_NAME")
val isJenkins = branchName != null
val dockerRegistryHost = if (isJenkins) alq.getEnvOrPropertyOrThrow("DOCKER_REGISTRY_HOST") else null
val dockerImageName = "aliquam/${artifactId}"

java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
//	implementation("org.aliquam:alq-cum:0.0.1-DEV_BUILD")
//	implementation("org.aliquam:alq-cum-service:0.0.1-DEV_BUILD")
	implementation("org.aliquam:alq-config-api:0.0.1-DEV_BUILD")
	implementation("org.aliquam:alq-session-api:0.0.1-DEV_BUILD")

	implementation("io.github.resilience4j:resilience4j-retry:1.7.1")

//	implementation ("org.springframework.boot:spring-boot-starter-actuator")
	implementation ("org.springframework.boot:spring-boot-starter-webflux:2.5.5")
	implementation("org.springframework.cloud:spring-cloud-starter-gateway:3.0.4")
	implementation("org.springframework.cloud:spring-cloud-starter-sleuth:3.0.4")
	runtimeOnly("net.logstash.logback:logstash-logback-encoder:7.0")

	testImplementation("org.springframework.boot:spring-boot-starter-test:2.5.5")
}

sonarqube {
	if (isJenkins) {
		properties {
			property("sonar.projectKey", "aliquam20_${artifactId}")
			property("sonar.organization", "aliquam")
			property("sonar.host.url", "https://sonarcloud.io")
			property("sonar.branch.name", branchName!!)
			property("sonar.coverage.jacoco.xmlReportPaths", "$projectDir/build/reports/jacoco/test/jacocoTestReport.xml")
		}
	}
}

tasks.getByName<BootBuildImage>("bootBuildImage") {
	when (branchName) {
		"master" -> {
			imageName = "${dockerRegistryHost}/${dockerImageName}:latest"
			isPublish = true
		}
		"develop" -> {
			imageName = "${dockerRegistryHost}/${dockerImageName}:snapshot"
			isPublish = true
		}
		else -> {
			imageName = "${dockerImageName}:dev_build"
			isPublish = false
		}
	}

	if (isJenkins) {
		docker {
			publishRegistry {
				username = System.getenv("DOCKER_REGISTRY_USER")
				password = System.getenv("DOCKER_REGISTRY_PASS")
				url = "https://docker.jeikobu.net/v2/"
			}
		}
	}
}

tasks.getByName<org.springframework.boot.gradle.tasks.bundling.BootJar>("bootJar") {
	if (branchName == null) {
		classpath(configurations["developmentOnly"])
	}
}

tasks.test {
	finalizedBy(tasks.jacocoTestReport) // report is always generated after tests run
}

tasks.jacocoTestReport {
	reports {
		xml.isEnabled = true
		csv.isEnabled = false
	}
	dependsOn(tasks.test) // tests are required to run before generating the report
}

tasks.withType<Test> {
	useJUnitPlatform()
}
