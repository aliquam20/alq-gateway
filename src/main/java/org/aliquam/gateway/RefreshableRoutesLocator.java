package org.aliquam.gateway;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.GatewayFilterSpec;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.gateway.route.builder.UriSpec;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Flux;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * A gateway route resolver which is used for dynamically refresh routes during the application runtime.
 *
 * @author          boto
 * Creation Date    25th June 2018
 */
@Component
@Slf4j
public class RefreshableRoutesLocator implements RouteLocator {

    private final RouteLocatorBuilder builder;
    private final GatewayRoutesRefresher gatewayRoutesRefresher;

    private RouteLocatorBuilder.Builder routesBuilder;
    private Flux<Route> route = Flux.empty();

    @Autowired
    public RefreshableRoutesLocator(@NonNull final RouteLocatorBuilder builder,
                                    @NonNull final GatewayRoutesRefresher gatewayRoutesRefresher) {
        this.builder = builder;
        this.gatewayRoutesRefresher = gatewayRoutesRefresher;

        clearRoutes();
    }

    /**
     * Remove all routes.
     */
    public void clearRoutes() {
        routesBuilder = builder.routes();
        addRoute("session", AlqGatewayRoutes.ALQ_HOST_SESSION);
        // We need to know the config service information.
        // Other services are stored in there so we'll initialize them later
        addRoute("config", AlqGatewayRoutes.ALQ_HOST_CONFIG);
        this.route = routesBuilder.build().getRoutes();
    }

    public void addRoute(String service, String serviceHost) {
        log.info("Adding route: {} to {}", service, serviceHost);
        routesBuilder
                .route(service + "_root", r -> r.path("/" + service)
                .uri("http://" + serviceHost + "/" + service))
                .route(service, r -> r
                        .path("/" + service + "/**")
                        .filters(rw -> rw.rewritePath("/" + service + "/(?<segment>.*)", "/" + service + "/${segment}"))
                        .uri("http://" + serviceHost + "/" + service)
                );
    }

    /**
     * Call this method in order to publish all routes defined by 'addRoute' calls.
     */
    public void buildRoutes() {
        this.route = routesBuilder.build().getRoutes();
        gatewayRoutesRefresher.refreshRoutes();
    }

    @Override
    public Flux<Route> getRoutes() {
        System.out.println("GET ROUTES CALLED");
        return route;
    }
}