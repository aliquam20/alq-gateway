package org.aliquam.gateway;

import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Import;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.retry.RetryContext;

import java.util.concurrent.Callable;

@SpringBootApplication
//@Import(org.aliquam.cum4service.SpringBootConfigurationReference.class)
@RestController
@Slf4j
public class AlqGatewayServiceApplication {

	private final RefreshableRoutesLocator refreshableRoutesLocator;
	private final AlqGatewayRoutes alqGatewayRoutes;

	public AlqGatewayServiceApplication(RefreshableRoutesLocator refreshableRoutesLocator, AlqGatewayRoutes alqGatewayRoutes) {
		this.refreshableRoutesLocator = refreshableRoutesLocator;
		this.alqGatewayRoutes = alqGatewayRoutes;
	}

	@GetMapping("/alqtest")
	public ResponseEntity<String> alqtest() {
		return ResponseEntity.ok().body("heyoo");
	}

	@EventListener(ApplicationReadyEvent.class)
	public void refreshRoutesAfterStartup() {
//		log.info("Initializing routes phase 1");
//		alqGatewayRoutes.addCoreRoutes(refreshableRoutesLocator);
//		refreshableRoutesLocator.buildRoutes();

		RetryConfig retryConfig = RetryConfig.custom()
				.maxAttempts(30)
				.intervalFunction(numAttempts -> 1000L)
				.build();
		Retry retry = Retry.of("Initialize routes phase 2", retryConfig);

		retry.getEventPublisher()
				.onRetry(event -> log.info("Call failed but will be retried in {}", event.getWaitInterval()));

		Runnable decoratedRequest = Retry.decorateRunnable(retry, this::initializeRoutesPhase2);
		decoratedRequest.run();
	}

	private void initializeRoutesPhase2() {
		log.info("Initializing routes phase 2");
		alqGatewayRoutes.addServiceRoutes(refreshableRoutesLocator);
		refreshableRoutesLocator.buildRoutes();
	}

	public static void main(String[] args) {
		SpringApplication.run(AlqGatewayServiceApplication.class, args);
	}
}
