package org.aliquam.gateway;

import com.fasterxml.jackson.core.type.TypeReference;
import org.aliquam.config.api.connector.AlqConfigConnector;
import org.aliquam.gateway.model.Host;
import org.aliquam.session.api.AlqSessionApi;
import org.aliquam.session.api.connector.AlqDummySessionManager;
import org.aliquam.session.api.connector.AlqSessionManager;
import org.aliquam.session.api.model.Session;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AlqGatewayRoutes {
    public static final String ALQ_HOST_SESSION = System.getenv("ALQ_HOST_SESSION") != null
            ? System.getenv("ALQ_HOST_SESSION") : "localhost:20401";
    public static final String ALQ_HOST_CONFIG = System.getenv("ALQ_HOST_CONFIG") != null
            ? System.getenv("ALQ_HOST_CONFIG") : "localhost:20501";

    private final AlqConfigConnector configConnector;

    public AlqGatewayRoutes() {
        AlqSessionManager sessionManager = AlqSessionApi.provideAlqSessionManager();
        configConnector = new AlqConfigConnector(sessionManager);
    }

    public void addServiceRoutes(RefreshableRoutesLocator refreshableRoutesLocator) {
        List<Host> hosts = configConnector.get("alq-gateway.hosts", new TypeReference<List<Host>>(){});
        for(Host host : hosts) {
            refreshableRoutesLocator.addRoute(host.getName(), host.getTarget());
        }
    }
}
