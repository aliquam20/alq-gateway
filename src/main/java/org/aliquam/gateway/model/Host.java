package org.aliquam.gateway.model;

import lombok.Data;

@Data
public class Host {
    private String name;
    private String target;
}
